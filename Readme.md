# Malá práce

## Úvod

Tento projekt je repozitář na Gitlabu, který používá generátor statických stránek ve spojení s kontinuální integrací (CI). CI generuje HTML stránky z Markdown podkladu a také testuje kvalitu Markdown souborů.

## Generátor statických stránek

Tento projekt používá generátor statických stránek (např. MKdocs, Middleman, Jekyll, atd.). Vyberal jsem si generátor, který mi nejlépe vyhovoval.

## Kontinuální integrace

V tomto projektu je nastavena kontinuální integrace. CI má minimálně dvě úlohy: test kvality Markdown stránek a generování HTML stránek z Markdown zdrojů. CI také automaticky nasazuje webové stránky.

## Jak se používá

1. Naklonujte tento repozitář.
2. Nainstalujte potřebné závislosti.
3. Spusťte skript pro generování stránek.
4. Proveďte testy kvality Markdown souborů.

## Práce s Markdown

Markdown je lehký a snadno použitelný syntax pro stylizaci našeho psaní. Tento projekt používá Markdown pro generování statických stránek.

### Syntaxe Markdown

Zde je rychlý přehled syntaxe Markdown, kterou můžeme použít v tomto projektu:

- Nadpisy: `# H1`, `## H2`, `### H3`, atd.
- Tučně: `**tučně**`
- Kurzíva: `*kurzíva*`
- Odkazy: `odkaz`
- Obrázky: `!alt text`
- Seznamy: `- položka seznamu`

### Testování kvality Markdown

Tento projekt obsahuje nástroje pro testování kvality našich Markdown souborů. Tyto nástroje kontrolují, zda naše soubory splňují standardy pro psaní v Markdown.

### Generování HTML stránek

Po úspěšném testování kvality vašich Markdown souborů CI generuje HTML stránky. Tyto stránky jsou automaticky nasazeny na webové stránky našeho projektu.

